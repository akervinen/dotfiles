# set XDG dirs
[[ -n "$XDG_CONFIG_HOME" ]] || export XDG_CONFIG_HOME="$HOME"/.config
[[ -n "$XDG_CACHE_HOME" ]] || export XDG_CACHE_HOME="$HOME"/.cache
[[ -n "$XDG_DATA_HOME" ]] || export XDG_DATA_HOME="$HOME"/.local/share

# move other .zsh files to XDG config
if [ -d "$XDG_CONFIG_HOME"/zsh ]; then
   export ZDOTDIR="$XDG_CONFIG_HOME"/zsh
   export ZSH_COMPDUMP="$XDG_CACHE_HOME"/zsh/zcompdump
fi

source "$ZDOTDIR"/.zshenv
