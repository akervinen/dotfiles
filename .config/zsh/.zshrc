# history
HISTSIZE=1000
HISTFILE="$XDG_DATA_HOME"/zsh/history
SAVEHIST=1000
setopt hist_ignore_dups \
       hist_ignore_space \
       inc_append_history

[[ -d "$XDG_DATA_HOME"/zsh ]] || mkdir -p "$XDG_DATA_HOME"/zsh
[[ -d "$XDG_CACHE_HOME"/zsh ]] || mkdir -p "$XDG_CACHE_HOME"/zsh

# for WSL
[[ -n $WSL ]] && export DISPLAY=:0.0

# stuffff
function cmdexists () {
    command -v "$1" >/dev/null 2>&1
}

# dircolors
source <(dircolors -b "$XDG_CONFIG_HOME"/dircolors.solarized-dark)

# functions
fpath=("$ZDOTDIR"/zfunc $fpath)

# cargo completion
cmdexists rustup && \
    fpath=($(rustc --print sysroot)/share/zsh/site-functions $fpath)

# completion
autoload -Uz compinit
function () {
    local dump="$ZSH_COMPDUMP"
    setopt extendedglob
    if [[ -n "$dump"(#qN.m1) ]]; then
        compinit -i -d "$dump"
        if [[ -s "$dump" && (! -s "$dump.zwc" || "$dump" -nt "$dump.zwc") ]]; then
            zcompile "$dump"
        fi
    else
        compinit -C
    fi
    unsetopt extendedglob
}

setopt always_to_end \
       complete_aliases \
       complete_in_word

zstyle ':completion:*:options' list-colors '=^(-- *)=34'

# styles copied from oh-my-zsh
zstyle ':completion:*:*:*:*:*' menu select

zstyle ':completion:*' list-colors ''

zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#) ([0-9a-z-]#)*=01;34=0=01'
zstyle ':completion:*:*:*:*:processes' command "ps -u $USER -o pid,user,comm -w -w"

zstyle ':completion::complete:*' use-cache 1
zstyle ':completion::complete:*' cache-path "$XDG_CACHE_HOME"/zsh/zcompcache

# disable named-directories autocompletion
zstyle ':completion:*:cd:*' tag-order local-directories directory-stack path-directories

# Don't complete uninteresting users
zstyle ':completion:*:*:*:users' ignored-patterns \
       adm amanda apache at avahi avahi-autoipd beaglidx bin cacti canna \
       clamav daemon dbus distcache dnsmasq dovecot fax ftp games gdm \
       gkrellmd gopher hacluster haldaemon halt hsqldb ident junkbust kdm \
       ldap lp mail mailman mailnull man messagebus  mldonkey mysql nagios \
       named netdump news nfsnobody nobody nscd ntp nut nvidia-persistenced nx \
       obsrun openvpn operator pcap polkitd postfix postgres privoxy pulse \
       pvm quagga radvd rpc rpcuser rpm rtkit scard shutdown squid sshd statd \
       svn sync 'systemd-*' tftp usbmux uucp uuidd vcsa wwwrun xfs '_*'

# ... unless we really want to.
zstyle '*' single-ignored show

# (s.:.) = split LS_COLORS by colons
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}

# colors
autoload -Uz colors
colors

# prompt
autoload -Uz promptinit
promptinit

# this is overwritten by loaded plugins
prompt walters

# options
setopt auto_pushd \
       pushd_ignore_dups
setopt noflowcontrol
setopt no_beep

# keys
bindkey -e

# from http://zshwiki.org/home/zle/bindkeys
# setup term keys
typeset -A key

key[Home]="$terminfo[khome]"
key[End]="$terminfo[kend]"
key[Insert]="$terminfo[kich1]"
key[Backspace]="$terminfo[kbs]"
key[Delete]="$terminfo[kdch1]"
key[Up]="$terminfo[kcuu1]"
key[Down]="$terminfo[kcud1]"
key[Left]="$terminfo[kcub1]"
key[Right]="$terminfo[kcuf1]"
key[PageUp]="$terminfo[kpp]"
key[PageDown]="$terminfo[knp]"

# setup key accordingly
[[ -n "$key[Home]"      ]] && bindkey -- "$key[Home]"      beginning-of-line
[[ -n "$key[End]"       ]] && bindkey -- "$key[End]"       end-of-line
[[ -n "$key[Insert]"    ]] && bindkey -- "$key[Insert]"    overwrite-mode
[[ -n "$key[Backspace]" ]] && bindkey -- "$key[Backspace]" backward-delete-char
[[ -n "$key[Delete]"    ]] && bindkey -- "$key[Delete]"    delete-char
[[ -n "$key[Up]"        ]] && bindkey -- "$key[Up]"        up-line-or-history
[[ -n "$key[Down]"      ]] && bindkey -- "$key[Down]"      down-line-or-history
[[ -n "$key[Left]"      ]] && bindkey -- "$key[Left]"      backward-char
[[ -n "$key[Right]"     ]] && bindkey -- "$key[Right]"     forward-char

# Finally, make sure the terminal is in application mode, when zle is
# active. Only then are the values from $terminfo valid.
if (( ${+terminfo[smkx]} )) && (( ${+terminfo[rmkx]} )); then
    function zle-line-init () {
        echoti smkx
    }
    function zle-line-finish () {
        echoti rmkx
    }
    zle -N zle-line-init
    zle -N zle-line-finish
fi

# custom keys

# ctrl+left/right to go back/frwd words
bindkey ';5D' emacs-backward-word
bindkey ';5C' emacs-forward-word

# ctrl+backspace to delete word
bindkey '^H' backward-delete-word

# wsltty ctrl+backspace and ctrl+del for deleting words
bindkey '\e[3;5~' kill-word
bindkey '\C-_' backward-kill-word

# add / and . to word delimiters
WORDCHARS=${WORDCHARS:s,/,,}
WORDCHARS=${WORDCHARS:s,.,,}

# "command not found" hook
# Ubuntu
[[ -e /etc/zsh_command_not_found ]] && source /etc/zsh_command_not_found
# Arch
[[ -e /usr/share/doc/pkgfile/command-not-found.zsh ]] && source /usr/share/doc/pkgfile/command-not-found.zsh

# aliases
alias ls='ls --color=auto -F'
alias ll='ls -lh'
alias la='ll -a'
alias df='df -h'
alias md='mkdir'

cmdexists colordiff && alias diff='colordiff'

# gpg-agent
if [[ -z $WSL ]]; then
    export GPG_TTY=$(tty)
    cmdexists gpg-connect-agent && gpg-connect-agent updatestartuptty /bye >/dev/null
fi

# weasel-pageant
# for using pageant keys with WSL
function () {
    wpath="/mnt/c/Program Files/weasel-pageant/weasel-pageant"
    [[ -f "$wpath" ]] && \
        eval $("$wpath" -q -r)
}

alias em='emacsclient -c'

if cmdexists antibody; then
    alias antibody_replug='antibody bundle < "$ZDOTDIR/.zsh_plugins.txt" > "$ZDOTDIR/.zsh_plugins.sh"'
    [[ ! -a "$ZDOTDIR/.zsh_plugins.sh" ]] && antibody bundle < "$ZDOTDIR/.zsh_plugins.txt" > "$ZDOTDIR/.zsh_plugins.sh"
    source "$ZDOTDIR/.zsh_plugins.sh"
fi

# autosuggestions
# better color for solarized theme
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=10'
