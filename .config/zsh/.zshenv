# qt provider
export QT_QPA_PLATFORMTHEME=qt5ct

# common stuff
if grep -q "Microsoft" /proc/version; then
  export WSL=1
	export EDITOR=nvim
	export BROWSER='explorer.exe'
else
  unset WSL
	export EDITOR='emacsclient -c'
	export VISUAL='emacsclient -c'
	export PAGER=most
	export BROWSER=xdg-open
fi

[[ -n WSL ]] && export SHELL=`which zsh`

source "$ZDOTDIR"/.zpath
